const routes = [
  {
    path: "/",
    component: () => import("src/pages/login.vue"),
    name: "login",
  },
  {
    path: "/createproject01",
    component: () => import("src/pages/createproject1.vue"),
    name: "createproject01",
  },
  {
    path: "/createproject02",
    component: () => import("src/pages/createproject2.vue"),
    name: "createproject02",
  },

  {
    path: "/info/:id",
    component: () => import("src/pages/detailinfo.vue"),
    name: "info",
  },
  {
    path: "/sensor/:id",
    component: () => import("src/pages/detailsensor.vue"),
    name: "sensor",
  },
  {
    path: "/report/:id",
    component: () => import("src/pages/detailreport.vue"),
    name: "report",
  },
  {
    path: "/main",
    component: () => import("src/pages/mainmenu.vue"),
    name: "mainmenu",
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
